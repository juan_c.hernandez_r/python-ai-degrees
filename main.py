import tensorflow as tf 
import numpy as np


celsius = np.array([-40, -10, 0, 8, 15, 22, 38], dtype=float)
farenheit= np.array([-40, 14, 32, 46, 59, 72, 100], dtype=float)

lawyer = tf.keras.layers.Dense(units=1, input_shape=[1])
model = tf.keras.Sequential([lawyer])

model.compile(
    optimizer=tf.keras.optimizers.Adam(0.1),
    loss= "mean_squared_error"
)

print("Starting training...")
history = model.fit(celsius, farenheit, epochs=1000, verbose=False)
print("Finished training")

print("Internal variables of the model")
print(lawyer.get_weights())

data=""
while data != "x":
    data = float(input("Input the degree in celsius to calculate: "))
    result = model.predict([data])
    print("The result is " + str(result)+ "farenheit")



